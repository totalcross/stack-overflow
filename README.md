Some stuff that we have responded on [Stack Overflow](https://stackoverflow.com)
and [Stack Overflow em Português](https://pt.stackoverflow.com).

If you find some issue with the codes, go ahead and comment in the
question/answer related!

-----------------------------------------------

Algumas coisas que nós respondemos no [Stack Overflow](https://stackoverflow.com)
e no [Stack Overflow em Português](https://pt.stackoverflow.com).

Se você encontrar alguma coisa no código, vai lá e comenta na resposta ou na
pergunta relaciona!