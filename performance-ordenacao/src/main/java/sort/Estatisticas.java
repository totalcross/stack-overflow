package sort;

import java.util.ArrayList;

public class Estatisticas {
	private ArrayList<Long> rodadas = new ArrayList<>();
	private Double buffMedia;
	private Double buffDesvio;
	private long buffTotal;

	public Estatisticas() {
	}

	public long totalRodadas() {
		return buffTotal;
	}

	public double getMedia() {
		if (buffMedia == null) {
			buffMedia = (1.0 * totalRodadas()) / numeroRepeticoes();
		}

		return buffMedia;
	}

	public double getDesvioPadrao() {
		if (buffDesvio == null) {
			if (numeroRepeticoes() <= 1) {
				buffDesvio = 0.0;
				return buffDesvio;
			}
			double media = getMedia();
			double somaQuadDiffs = 0;

			for (Long l : rodadas) {
				double dEquiv = l;
				double diff = dEquiv - media;

				somaQuadDiffs += diff * diff;
			}

			buffDesvio = Math.sqrt(somaQuadDiffs / (numeroRepeticoes() - 1));
		}
		return buffDesvio;
	}
	
	public int numeroRepeticoes() {
		return rodadas.size();
	}

	public void adicionaRodada(long l) {
		buffMedia = null;
		buffDesvio = null;
		buffTotal += l;

		rodadas.add(l);
	}
	
	public static void printEstatisticas(Estatisticas estat, String sobre) {
		System.out.println("Repetições do experimento de " + sobre + ": " + estat.numeroRepeticoes());
		System.out.println("Total acumulado de " + sobre + ": " + estat.totalRodadas());
		System.out.println("Média de " + sobre + ": " + estat.getMedia());
		System.out.println("Desvio padrão de " + sobre + ": " + estat.getDesvioPadrao());
	}
}
